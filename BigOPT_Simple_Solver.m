
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script uses the default Genetic Algorithm implemented in Matlab.
% correlation Matrix.
%
% Uncomment only one dataset, Or modify the script accordingly
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format longE;

col = 256;

%[A, S, X] = Load_BigOPT_Instance('D4'); row = 4; % Comment to not use D4N


%[A, S, X] = Load_BigOPT_Instance('D4N'); row = 4; % Uncomment to use D4N


[A, S, X] = Load_BigOPT_Instance('D12'); row = 12; % Uncomment to use D12


%[A, S, X] = Load_BigOPT_Instance('D12N'); row = 12; % Uncomment to use D12N


%[A, S, X] = Load_BigOPT_Instance('D19'); row = 19; % Uncomment to use D19


%[A, S, X] = Load_BigOPT_Instance('D19N'); row = 19; % Uncomment to use D19N

size = row * col;

options = optimoptions('ga', 'PlotFcn', {@gaplotdistance,@gaplotrange}, 'MaxGenerations', 30);

FitnessFunction = @(candidate_Solution) Calculate_Single_Objective_Value(candidate_Solution, S, A, X);

[x,fval,exitflag,output,population,scores] = ga(FitnessFunction, size, [],[],[],[],-7.99, 7.99,[],options);


